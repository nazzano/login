import { login } from "./functions/users";

const email = document.querySelector('input[name="email"]');
const password = document.querySelector('input[name="password"]');

//login(email, password, data => {
//   console.log()
//})

const form = document.querySelector("form");

form.addEventListener("submit", e => {
  e.preventDefault();
  login(email.value, password.value).then(data => console.log(data));
});
