export const login = (email, password) => {
  return fetch("https://reqres.in/api/login", {
    body: JSON.stringify({
      email,
      password,
    }),
    headers: {
      "Content-Type": "application/json; charset=UTF-8",
    },
    method: "POST",
  }).then(res => res.json());
  //.then(data => cb(data));
};
